import datetime
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,
    jsonify, make_response
)
from tvideos.database import DB
from tvideos.models.pelicula import Pelicula
from tvideos.models.copia_pelicula import Copia_pelicula
from tvideos.models.cliente import Cliente
from tvideos.models.prestamo_pelicula import Prestamo_pelicula

from bson.objectid import ObjectId
import traceback

tvideosBp = Blueprint('tvideos', __name__, url_prefix='')

@tvideosBp.route('/')
def menuPrincipal():
    return render_template('index.html', title='Videoclub - Administración', bdconn=DB.status())

@tvideosBp.route('/peliculas', methods=['GET'])
def peliculas():
    # funcion lista de peliculas
    peliculas = []
    _peliculas = DB.find('peliculas')
    for pelicula in _peliculas:
        #print(pelicula['_id'])
        #print(pelicula['_id'], ':',pelicula['actores_principales'])
        unidades = DB.find('copias_peliculas', { 'pelicula': pelicula['_id'] }).count()
        pelicula['unidadesAdquiridas'] = unidades
        peliculas.append(pelicula)
    d = {
        'peliculas': list(peliculas),
        'total': len(peliculas)
    }
    return render_template('peliculas.html',
                           title='Películas',
                           bdconn=DB.status(),
                           d=d)


@tvideosBp.route('/pelicula/add', methods=('GET', 'POST'))
def nuevaPelicula():
    if request.method == 'GET':
        return render_template('nueva_pelicula.html', title="Registrar nueva película", bdconn=DB.status(),)
    if request.method == 'POST':
        titulo = request.form.get('titulo', None)
        if titulo is None:
            print('Se necesita un titulo')
            return redirect('/pelicula/add', code=304)
        genero = request.form.get('genero', None)
        if genero is None:
            print('Se necesita un genero')
            return redirect('/pelicula/add', code=304)
        duracion = request.form.get('duracion', None)
        if duracion is None:
            print('Se necesita duracion')
            return redirect('/pelicula/add', code=304)
        año = request.form.get('año', None)
        if año is None:
            print('Se necesita año')
            return redirect('/pelicula/add', code=304)
        compañía = request.form.get('compañía', None)
        if compañía is None:
            print('Se necesita compañía')
            return redirect('/pelicula/add', code=304)
        actor_principal = request.form.get('actor_principal', '')
        rol_actor = request.form.get('rol_actor', '')
        nominacion_premio = request.form.get('nominacion_premio', '')
        nominacion_año = request.form.get('nominacion_año', '')
        nominacion_detalle = request.form.get('nominacion_detalle', '')
        nominacion_gano = False
        if request.form['nominacion_gano'] == 'si':
            nominacion_gano = True

        nominaciones = []
        nominaciones.append({
            'nombre_premio': nominacion_premio,
            'detalle': nominacion_detalle,
            'año': nominacion_año,
            'ganado': nominacion_gano
        })
        actores_principales = []
        actores_principales.append({
            'nombre': actor_principal,
            'interpretacion': rol_actor
        })
        try:
            pelicula = Pelicula(titulo=titulo,
                                genero=genero,
                                duracion=duracion,
                                año=año,
                                nominaciones=nominaciones,
                                actores_principales=actores_principales,
                                compañía=compañía)
            pelicula.insert()
            inserted = DB.find_one('peliculas',
                                   {
                                       'titulo':titulo ,
                                       'duracion': duracion,
                                       'genero': genero
                                   })
            print('inserted:::', inserted)
            return redirect('/pelicula/editar/' + str(inserted['_id']), code=302)
        except Exception as E:
            print('Error insertando pelicula', E)
            traceback.print_exc()
            return redirect('/pelicula/add', code=304)
        
@tvideosBp.route('/pelicula/editar/<string:_id>', methods=('GET', 'POST'))
def editarPelicula(_id):
    # funcion para editar una pelicula
    if request.method == 'GET':
        pelicula = DB.find_one('peliculas', { '_id': ObjectId(_id) })
        unidades = DB.find('copias_peliculas', { 'pelicula': pelicula['_id'] })
        d = {
            'pelicula': pelicula,
            'unidades': list(unidades)
        }
        return render_template('pelicula.html', title='Editar Película', d=d, bdconn=DB.status(),)
    if request.method == 'POST':
        pelicula = DB.find_one('peliculas', { '_id': ObjectId(_id) })
        if pelicula.get('titulo', None) is None:
            return redirect('/pelicula/editar/'+str(_id), code=304)
        titulo = request.form.get('titulo')
        genero = request.form.get('genero')
        duracion = request.form.get('duracion')
        año = request.form.get('año')
        nominaciones = request.form.get('nominaciones')
        actores_principales = request.form.get('actores_principales')
        costo_unitario = request.form.get('costo_unitario')
        compañía = request.form.get('compañía')
        print('pelicula', pelicula)
        try:
            DB.update_one('peliculas', {'_id': pelicula['_id']},
                          {
                              '$set': {
                                  'titulo': titulo,
                                  'genero': genero,
                                  'duracion': duracion,
                                  'año': año,
                                  'nominacions': nominaciones,
                                  'actores_principales': actores_principales,
                                  'costo_unitario': costo_unitario,
                                  'compañía': compañía
                              }
                          })
        except Exception as E:
            print('Error editando pelicula', str(E))
            return redirect('/pelicula/editar/'+str(_id), code=304)
        return redirect('/pelicula/editar/'+str(_id), code=302)

@tvideosBp.route('/copia_pelicula/add/<string:_id>', methods=('GET', 'POST'))
def nuevaCopiaPelicula(_id):
    try:
        copia_pelicula = Copia_pelicula(pelicula=ObjectId(_id))
        copia_pelicula.insert()
    except Exception as E:
        print('Error al crear copia de pelicula', str(E))
        return redirect('/pelicula/editar/'+str(_id), code=304)
    return redirect('/pelicula/editar/'+str(_id), code=302)

@tvideosBp.route('/copia_pelicula/baja/<string:_id>', methods=('GET', 'POST'))
def bajaCopiaPelicula(_id):
    copia_pelicula = DB.find_one('copias_peliculas', { '_id': ObjectId(_id) })
    pelicula = DB.find_one('peliculas', { '_id': copia_pelicula['pelicula'] })
    if copia_pelicula.get('pelicula', None) is None or pelicula.get('titulo', None) is None:
        return redirect('peliculas/', code=304)
    if request.method == 'GET':
        d = {
            'pelicula': pelicula,
            'copia_pelicula': copia_pelicula
        }
        return render_template('baja_copia_pelicula.html', title='Registrar Baja de copia de película',
                               d=d, bdconn=DB.status(),)
    if request.method == 'POST':
        print(request.form)
        nuevaBaja = {
            'razon': request.form.get('razon'),
            'fecha': request.form.get('año') + '/' + request.form.get('mes') + '/' + request.form.get('dia')
        }
        bajas = copia_pelicula['bajas']
        bajas.append(nuevaBaja)
        try:
            DB.update_one('copias_peliculas', {'_id': copia_pelicula['_id']},
                          {
                              '$set': {
                                  'bajas': bajas
                              }
                          })
        except Exception as E:
            print('Error actualizando copias_peliculas:', str(E))
            return redirect('peliculas/', code=304)
    return redirect('/pelicula/editar/'+ str(pelicula['_id']), code=302)
        
@tvideosBp.route('/clientes', methods=['GET'])
def clientes():
    # funcion para lista de clientes
    clientes = []
    _clientes = DB.find('clientes')
    for cliente in _clientes:
        o = {
            '_id': cliente['_id'],
            'nombres': cliente.get('nombres', ''),
            'ap_paterno': cliente.get('ap_paterno', ''),
            'ap_materno': cliente.get('ap_materno', ''),
            'ci': cliente.get('ci', ''),
            'fecha_nacimiento': cliente.get('fecha_nacimiento', ''),
            'email': cliente.get('email', ''),
            'direccion': cliente.get('direccion', ''),
            'geolocalizacion': cliente.get('geolocalizacion', ''),
            'betado': cliente.get('betado', ''),
        }
        clientes.append(o)
    d = {
        'clientes': clientes,
        'total': len(clientes)
    }
    return render_template('clientes.html', title='Clientes', d=d, bdconn=DB.status(),)

@tvideosBp.route('/cliente/add', methods=['GET', 'POST'])
def nuevoCliente():
    if request.method == 'GET':
        return render_template('nuevo_cliente.html', title='Registrar nuevo cliente', bdconn=DB.status(),)
    if request.method == 'POST':
        nombres = request.form.get('nombres', '')
        ap_paterno = request.form.get('ap_paterno', '')
        ap_materno = request.form.get('ap_materno', '')
        ci = request.form.get('ci', '')
        fecha_nacimiento = request.form.get('fecha_nacimiento', '')
        email = request.form.get('email', '')
        direccion = request.form.get('direccion', '')
        latitud = request.form.get('latitud', '')
        longitud = request.form.get('longitud', '')
        
        try:
            cliente = Cliente(nombres=nombres,
                              ap_paterno=ap_paterno,
                              ap_materno=ap_materno,
                              ci=ci,
                              fecha_nacimiento=fecha_nacimiento,
                              email=email,
                              direccion=direccion,
                              latitud=latitud,
                              longitud=longitud)
            cliente.insert()
            inserted = DB.find_one('clientes', { 'ci': ci })
            print(inserted)
            return redirect('/cliente/edit/'+ str(inserted['_id']), code=302)
        except Exception as E:
            print('Error registrando nuevo cliente', str(E))
            return redirect('/clientes', code=304)
        
@tvideosBp.route('/cliente/edit/<string:_id>', methods=('GET', 'POST'))
def editarCliente(_id):
    if request.method == 'GET':
        cliente = DB.find_one('clientes', { '_id': ObjectId(_id) })
        cliente['latitud'] = cliente.get('geolocalizacion',',').split(',')[0]
        cliente['longitud'] = cliente.get('geolocalizacion', ',').split(',')[1]
        print(cliente)
        d = {
            'cliente': cliente
        }
        return render_template('cliente.html', title='Editar datos de cliente', d=d, bdconn=DB.status())
    if request.method == 'POST':
        cliente = DB.find_one('clientes', { '_id': ObjectId(_id) })
        nombres = request.form.get('nombres', '')
        ap_paterno = request.form.get('ap_paterno', '')
        ap_materno = request.form.get('ap_materno', '')
        ci = request.form.get('ci', '')
        fecha_nacimiento = request.form.get('fecha_nacimiento', '')
        email = request.form.get('email', '')
        direccion = request.form.get('direccion', '')
        latitud = request.form.get('latitud', '')
        longitud = request.form.get('longitud', '')

        try:
            DB.update_one('clientes', { '_id': cliente['_id']},
                          {
                              '$set': {
                                  'nombres': nombres,
                                  'ap_paterno': ap_paterno,
                                  'ap_materno': ap_materno,
                                  'ci': ci,
                                  'fecha_nacimiento': fecha_nacimiento,
                                  'email': email,
                                  'direccion': direccion,
                                  'geolocalizacion': latitud + ',' + longitud
                              }
                          }
            )
        except Exception as E:
            print('Error editando cliente', str(E))
            return redirect('/cliente/edit/'+str(_id), code=304)
        return redirect('/cliente/edit/'+str(_id), code=302)

@tvideosBp.route('/cliente/baja/<string:_id>', methods=('GET', 'POST'))
def bajaCliente(_id):
    if request.method == 'POST':
        cliente = DB.find_one('clientes', { '_id': ObjectId(_id) })
        razon = request.form.get('razon', 'No especificada')
        fecha = str(datetime.datetime.now())
        _bajas = list(cliente.get('bajas', []))
        bajas = []
        for baja in _bajas:
            bajas.append(baja)
        bajas.append({'razon': razon, 'fecha': fecha })
        try:
            DB.update_one('clientes', { '_id': cliente['_id']},
                          {
                              '$set': {
                                  'bajas': bajas,
                                  'betado': True
                                  }
                          })
                          
        except Exception as E:
            print('Error dando de baja a cliente', str(E))
            return redirect('/cliente/edit/' + str(_id), code=304)
        return redirect('/cliente/edit/' + str(_id), code=302)

@tvideosBp.route('/cliente/alta/<string:_id>', methods=('GET', 'POST'))
def altaCliente(_id):
    if request.method == 'POST':
        cliente = DB.find_one('clientes', { '_id': ObjectId(_id) })
        razon = request.form.get('razon', 'No especificada')
        fecha = datetime.datetime.now().strftime("%d/%m/%Y")
        _altas = list(cliente.get('altas', []))
        altas = []
        for alta in _altas:
            altas.append(alta)
        altas.append({'razon': razon, 'fecha': fecha })
        try:
            DB.update_one('clientes', { '_id': cliente['_id'] },
                          {
                              '$set': {
                                  'altas': altas,
                                  'betado': False
                                  }
                          })
        except Exception as E:
            print('Error dando de alta a cliente', str(E))
            return redirect('/cliente/edit/' + str(_id), code=304)
        return redirect('/cliente/edit/' + str(_id), code=302)

@tvideosBp.route('/prestamos', methods=['GET'])
def prestamos():
    _prestamos_peliculas = DB.find('prestamos_peliculas')
    prestamos_peliculas = []
    for prestamo in _prestamos_peliculas:
        o = {}
        try:
            cliente = DB.find_one('clientes', { '_id': prestamo['cliente'] })
            o['cliente'] = cliente['_id']
            o['cliente_nombres'] = cliente['nombres']
            o['cliente_ap_paterno'] = cliente['ap_paterno']
            o['cliente_ap_materno'] = cliente['ap_materno']
            o['cliente_ci'] = cliente['ci']
        except:
            o['cliente_nombres'] = ''
            o['cliente_ap_paterno'] = ''
            o['cliente_ap_materno'] = ''
            o['cliente_ci'] = ''
        cp = []
        for copia_pelicula in prestamo['copias_peliculas']:
            try:
                copia = DB.find_one('copias_peliculas', { '_id': copia_pelicula['copia_pelicula'] })
                pelicula = DB.find_one('peliculas', { '_id': ObjectId(copia['pelicula']) })
                cp.append({
                    'copia_pelicula': pelicula['_id'],
                    'copia_pelicula_titulo': pelicula['titulo']
                })
            except Exception as E:
                print('Error consultando pelicula ', str(E))
                cp.append('copia_pelicula')
                cp['copia_pelicula'] = pelicula['_id']
                cp.append({
                    'copia_pelicula': ObjectId(pelicula['_id']),
                    'copia_pelicula_titulo': ''
                })
        o['_id'] = prestamo['_id']
        o['copias_peliculas'] = cp
        o['fecha_prestamo'] = prestamo['fecha_prestamo']
        o['fecha_devolucion'] = prestamo['fecha_devolucion']
        o['dias_prestamo'] = prestamo['dias_prestamo']
        o['retornado'] = prestamo['retornado']
        if prestamo.get('pago', None) is not None:
            o['pago'] = prestamo['pago']
        o['costo_facturar'] = calcularMontoAFacturar(o)
        o['creacion'] = prestamo['creacion']
        prestamos_peliculas.append(o)
    return render_template('prestamos.html',
                           title='Pŕestamos de películas',
                           bdconn=DB.status(),
                           d={
                               'prestamos_peliculas': prestamos_peliculas,
                               'total': len(prestamos_peliculas)
                           })

@tvideosBp.route('/prestamo/add', methods=['GET', 'POST'])
def nuevoPrestamo():
    if request.method == 'GET':
        _copias_peliculas = DB.find('copias_peliculas', { 'prestado': False })
        copias_peliculas = []
        for copia_pelicula in _copias_peliculas:
            if len(copia_pelicula.get('bajas', [])) == 0:
                pelicula = DB.find_one('peliculas', { '_id': copia_pelicula['pelicula'] })
                copias_peliculas.append({
                    '_id': copia_pelicula['_id'],
                    'pelicula': pelicula['_id'],
                    'titulo': pelicula['titulo'],
                    'genero': pelicula['genero'],
                    'año': pelicula['año'],
                })
        clientes = DB.find('clientes', { 'betado': False })
        d = {
            'clientes': list(clientes),
            'copias_peliculas': copias_peliculas,
            'dias_maximo_prestamo': diasMaximoPrestamo()
        }
        return render_template('nuevo_prestamo.html', title="Nuevo préstamo", d=d, bdconn=DB.status(),)
    if request.method == 'POST':
        # print('....::::::\n', list(request.form.keys()))
        _cliente = request.form.get('cliente', None)
        if _cliente is None:
            print('No se ha proporcionado cliente')
            return redirect('/prestamo/add', code=304)
        fecha_prestamo = request.form.get('fecha_prestamo', None)
        if fecha_prestamo is None:
            print('Se require fecha de prestamo')
            return redirect('/prestamo/add', code=304)
        dias_prestamo = request.form.get('dias_prestamo', None)
        _copias_peliculas = []
        # examinando lista de copias de peliculas
        for key in request.form.keys():
            if key.startswith('copia_pelicula_'):
                _copias_peliculas.append(request.form.get(key))
        # comprobando claves
        cliente = DB.find_one('clientes', { '_id': ObjectId(_cliente) })
        if cliente is None:
            print('No existe cliente con _id', _cliente)
            return redirect('/prestamo/add', code=304)
        copias_peliculas = []
        for _copia in _copias_peliculas:
            copia = DB.find_one('copias_peliculas', { '_id': ObjectId(_copia) })
            if copia is None:
                print('No existe copia de pelicula con _id', _copia)
                return redirect('/prestamo/add', code=304)
            copias_peliculas.append({ 'copia_pelicula': ObjectId(_copia) })
        # creando nuevo prestamo
        prestamo_pelicula = Prestamo_pelicula(
            cliente=ObjectId(_cliente),
            fecha_prestamo=fecha_prestamo,
            dias_prestamo=dias_prestamo,
            copias_peliculas=copias_peliculas)
        try:
            # marcando las copias_como prestados
            for copia_pelicula in copias_peliculas:
                # TODO: Deberia agregarse soporte para transacciones aqui
                try:
                    DB.update_one('copias_peliculas', { '_id': ObjectId(copia_pelicula['copia_pelicula']) },
                                  {
                                      '$set': {
                                          'prestado': True
                                      }
                                  })
                except Exception as E:
                    print('Error actualizando copia de peliculas', str(E))
                    # TODO: Aqui se deberia disparar la transaccion y hacer rollback
            prestamo_pelicula.insert()
            return redirect('/prestamos', code=302)
        except Exception as E:
            print('Error insertando pelicula', E)
            return redirect('/prestamos', code=302)
        
@tvideosBp.route('/prestamo/facturar/<string:_id>', methods=('GET', 'POST'))
def facturarPrestamo(_id):
    if request.method == 'GET':
        prestamo_pelicula = DB.find_one('prestamos_peliculas', { '_id':  ObjectId(_id) })
        if prestamo_pelicula is None:
            print('Error buscando prestamo de pelicula con id', _id)
            return redirect('/prestamos', code=304)
        costo_facturar = calcularMontoAFacturar(prestamo_pelicula)
        cliente = DB.find_one('clientes', { '_id': prestamo_pelicula['cliente'] })
        o = {}
        o['cliente'] = cliente['_id']
        o['cliente_nombres'] = cliente['nombres']
        o['cliente_ap_paterno'] = cliente['ap_paterno']
        o['cliente_ap_materno'] = cliente['ap_materno']
        o['cliente_ci'] = cliente['ci']
        o['dias_prestamo'] = prestamo_pelicula['dias_prestamo']
        o['fecha_prestamo'] = prestamo_pelicula['fecha_prestamo']
        o['_id'] = _id
        cp = []
        for copia_pelicula in prestamo_pelicula['copias_peliculas']:
            copia = DB.find_one('copias_peliculas', { '_id': copia_pelicula['copia_pelicula'] })
            pelicula = DB.find_one('peliculas', { '_id': ObjectId(copia['pelicula']) })
            cp.append({
                'copia_pelicula': pelicula['_id'],
                'copia_pelicula_titulo': pelicula['titulo']
            })
        o['copias_peliculas'] = cp
        # preparando registro de pago
        pago = {
            'fecha_pago': datetime.datetime.now().strftime("%d/%m/%Y"),
            'monto': costo_facturar
        }
        o['pago'] = pago
        try:
            DB.update_one('prestamos_peliculas', { '_id': ObjectId(prestamo_pelicula['_id']) },
                          {
                              '$set': {
                                  'pago': pago
                              }
                          }
            )
            print(o)
            return render_template('prestamo_factura.html',
                                   bdconn=DB.status(),
                                   title='Factura Préstamo película', prestamo=o)
        except Exception as E:
            print('Error actualizando prestamo de pelicula', str(E))
            traceback.print_exc()
            return redirect('/prestamos', code=302)

@tvideosBp.route('/prestamo/devolucion/<string:_id>', methods=('GET', 'POST'))
def devolucionPrestamo(_id):
    DB.status()
    if request.method == 'GET':
        prestamo_pelicula = DB.find_one('prestamos_peliculas', { '_id':  ObjectId(_id) })
        if prestamo_pelicula is None:
            print('Error buscando prestamo de pelicula con id', _id)
            return redirect('/prestamos', code=304)
        cliente = DB.find_one('clientes', { '_id': prestamo_pelicula['cliente'] })
        # actualizando copias peliculas
        for copia_pelicula in prestamo_pelicula['copias_peliculas']:
            copia = DB.find_one('copias_peliculas', { '_id': copia_pelicula['copia_pelicula'] })
            try:
                DB.update_one('copias_peliculas', { '_id': ObjectId(copia['_id']) },
                              {
                                  '$set': {
                                      'prestado': False
                                  }
                              }
                )
            except Exception as E:
                print('Error actualizando copias de peliculas', str(E))
                return redirect('/prestamos', code=304)
        # actualizando prestamo
        try:
            DB.update_one('prestamos_peliculas', { '_id': ObjectId(prestamo_pelicula['_id']) },
                          {
                              '$set': {
                                  'fecha_devolucion': datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                                  'retornado': True
                              }
                          }
            )
            return redirect('/prestamos', code=302)
        except Exception as E:
            print('Error actualizando prestamo de pelicula', str(E))
            traceback.print_exc()
            return redirect('/prestamos', code=304)

@tvideosBp.route('/costos', methods=('GET', 'POST'))
def costosYDescuentos():
    DB.status()
    if request.method == 'GET':
        _costos_y_descuentos = DB.find('costos_y_descuentos')[0]
        costos = []
        c = 0
        for costo in _costos_y_descuentos['costos']:
            costos.append({
                'id': c,
                'cantidad': costo['cantidad'],
                'costo': costo['costo']
            })
            c = c + 1
        descuentos = []
        c = 0
        for descuento in _costos_y_descuentos['descuentos']:
            descuentos.append({
                'id': c,
                'cantidad_minima': descuento['cantidad_minima'],
                'cantidad_maxima': descuento['cantidad_maxima'],
                'descuento': descuento['descuento']
            })
            c = c + 1
        return render_template('costos_y_descuentos.html', title='Edtiar Costos y descuentos',
                               costos_y_descuentos={ 'costos': costos, 'descuentos': descuentos },
                               bdconn=DB.status())
    if request.method == 'POST':
        print('::::', request.form)
        print('keys::::', list(request.form.keys()))
        print('values::::', list(request.form.values()))
        costos = [{}, {}, {}, {}, {}] # lista quemada de 5 elementos
        descuentos = [{}, {}] # lista quemada de 2 elementos
        for key in request.form.keys():
            if key.startswith('cantidad_'):
                n = int(key.split('cantidad_')[1])
                costos[n]['cantidad'] = int(request.form.get(key))
            if key.startswith('costo_'):
                n = int(key.split('costo_')[1])
                costos[n]['costo'] = float(request.form.get(key))

            if key.startswith('minima_'):
                n = int(key.split('minima_')[1])
                descuentos[n]['cantidad_minima'] = int(request.form.get(key))
            if key.startswith('maxima_'):
                n = int(key.split('maxima_')[1])
                descuentos[n]['cantidad_maxima'] = int(request.form.get(key))
            if key.startswith('descuento_'):
                n = int(key.split('descuento_')[1])
                descuentos[n]['descuento'] = float(request.form.get(key))
        print('costos:::', costos)
        print('descuentos:::', descuentos)
        try:
            costos_y_descuentos = DB.find('costos_y_descuentos')[0]
            DB.update_one('costos_y_descuentos', { '_id': ObjectId(costos_y_descuentos['_id']) },
                          {
                              '$set': {
                                  'costos': costos,
                                  'descuentos': descuentos
                              }
                          })
            return redirect('/costos', code=302)
        except Exception as E:
            print('Error actualizando costos y descuentos', E)
            return redirect('/costos', code=304)
        
# utilitarios varios
def calcularMontoAFacturar(prestamo_pelicula):
    costos_y_descuentos = DB.find('costos_y_descuentos')
    costos_y_descuentos = costos_y_descuentos[0]
    cantidadCopias = len(prestamo_pelicula['copias_peliculas'])
    costo = 0
    for _costo in costos_y_descuentos['costos']:
        # buscando cantidad fija
        if int(_costo['cantidad']) >= int(prestamo_pelicula['dias_prestamo']):
            costo = _costo['costo']
            break
    if costo == 0:
        # no se encuentra una cantidad de dias permitidos
        return -1
    else:
        # buscando descuento a aplicar
        descuento = 0
        for _descuento in costos_y_descuentos['descuentos']:
            if _descuento['cantidad_minima'] >= cantidadCopias and _descuento['cantidad_maxima'] <= cantidadCopias:
                descuento = _descuento['descuento']
                break
        return (costo*cantidadCopias) - (costo*cantidadCopias)*descuento

def diasMaximoPrestamo():
    costos_y_descuentos = DB.find('costos_y_descuentos')[0]
    costos = costos_y_descuentos['costos']
    cantidad = 0
    for costo in costos:
        if costo['cantidad'] > cantidad:
            cantidad = costo['cantidad']
    return int(cantidad)
