import datetime

from tvideos.database import DB
from bson.objectid import ObjectId

class Pelicula(object):
    def __init__(self, **kwargs):

        # obligatorios
        self.titulo = kwargs['titulo']
        self.genero = kwargs['genero']
        self.duracion = kwargs['duracion']
        self.año = kwargs['año']
        # opcionales
        if kwargs.get('nominaciones', None) is None:
            self.nominaciones = []
        else:
            self.nominaciones = kwargs['nominaciones']
        if kwargs.get('actores_principales', None) is None:
            self.actores_principales = []
        else:
            self.actores_principales = kwargs['actores_principales']
        if kwargs.get('costo_unitario', None) is None:
            self.costo_unitario = 10
        else:
            self.costo_unitario = kwargs[costo_unitario]
        if kwargs.get('compañía', None) is None:
            self.compañía = ""
        else:
            self.compañía = kwargs['compañía']

        self.created_at = datetime.datetime.now().strftime("%d/%m/%Y")

    def insert(self):
        if not DB.find_one("peliculas", {"titulo": self.titulo}):
            DB.insert(collection='peliculas', data=self.json())

    def json(self):
        return {
            'titulo': self.titulo,
            'genero': self.genero,
            'duracion': self.duracion,
            'año':  self.año,
            'nominaciones': self.nominaciones,
            'actores_principales': self.actores_principales,
            'costo_unitario': self.costo_unitario,
            'compañía': self.compañía,
            'created_at': self.created_at
        }
