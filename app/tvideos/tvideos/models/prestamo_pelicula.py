import datetime

from tvideos.database import DB
from bson.objectid import ObjectId

class Prestamo_pelicula(object):
    def __init__(self, **kwargs):
        if kwargs.get('cliente', None) is None:
            raise ValueError('Los préstamos se hacen con al menos un cliente')
        else:
            cliente = DB.find_one('clientes', { '_id': ObjectId(kwargs['cliente']) })
            if cliente is None:
                raise ValueError('No se ha encontrado cliente con _id', kwargs['cliente'])
        self.cliente = ObjectId(kwargs['cliente'])
        if kwargs.get('copias_peliculas', None) is not None:
            for copia_pelicula in kwargs['copias_peliculas']:
                copia = DB.find_one('copias_peliculas', { '_id': ObjectId(copia_pelicula['copia_pelicula']) })
                pelicula = DB.find_one('peliculas', { '_id': copia['pelicula'] })
                if pelicula.get('titulo', None) is None:
                    raise ValueError('No se encuentra el id de copia_pelicula', copia_pelicula['copia_pelicula'])
        else:
            raise ValueError('Los prestamos se hacen sobre al menos una copia de pelicula')
        self.copias_peliculas = kwargs['copias_peliculas']

        if kwargs.get('fecha_prestamo', None) is None:
            self.fecha_prestamo = datetime.datetime.now().strftime("%d/%m/%Y")
        else:
            self.fecha_prestamo = kwargs['fecha_prestamo']
            try:
                datetime.datetime.strptime(kwargs['fecha_prestamo'], '%d/%m/%Y')
                self.fecha_prestamo = kwargs['fecha_prestamo']
            except ValueError:
                raise ValueError("Incorrect data format %s, should be DD/MM/YYYY" % (kwargs['fecha_prestamo']))

        if kwargs.get('dias_prestamo', None) is None:
            self.dias_prestamo = 3
        else:
            self.dias_prestamo = kwargs['dias_prestamo']
            
        if kwargs.get('fecha_devolucion', None) is None:
            self.fecha_devolucion = ''
        else:
            try:
                datetime.datetime.strptime(kwargs['fecha_devolucion'], '%d/%m/%Y')
                self.fecha_prestamo = kwargs['fecha_devolucion']
            except ValueError:
                raise ValueError("Incorrect data format %s, should be DD/MM/YYYY" % (kwargs['fecha_devolucion']))
            self.fecha_devolucion = kwargs['fecha_devolucion']

        self.retornado = kwargs.get('retornado', False)
        self.creacion = datetime.datetime.now().strftime("%d/%m/%Y")

    def insert(self):
        DB.insert(collection='prestamos_peliculas', data=self.json())

    def json(self):
        return {
            'cliente': self.cliente,
            'copias_peliculas': self.copias_peliculas,
            'fecha_prestamo': self.fecha_prestamo,
            'dias_prestamo': self.dias_prestamo,
            'fecha_devolucion': self.fecha_devolucion,
            'retornado': self.retornado,
            'creacion': self.creacion
        }
