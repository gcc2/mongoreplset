import datetime

from tvideos.database import DB
from bson.objectid import ObjectId

class Copia_pelicula(object):
    def __init__(self, **kwargs):
        print(kwargs)
        if kwargs.get('pelicula', None) is None:
            raise ValueError('El documento Copia_Pelicula debe referenciar a una pelicula')
        else:
            pelicula = DB.find_one('peliculas', { '_id': kwargs['pelicula'] })
            if pelicula.get('titulo', None) is not None:
                self.pelicula = kwargs['pelicula']
            else:
                raise ValueError('No se ha encontrado la pelicula con _id', _id)
        if kwargs.get('bajas', None) is None:
            self.bajas = []
        else:
            self.bajas = kwargs['bajas']
        self.prestado = kwargs.get('prestado', False)
        self.creacion = datetime.datetime.now().strftime("%d/%m/%Y")
        

    def insert(self):
        DB.insert(collection='copias_peliculas', data=self.json())

    def json(self):
        return {
            'pelicula': self.pelicula,
            'creacion': self.creacion,
            'bajas': self.bajas,
            'prestado': self.prestado
        }
    
