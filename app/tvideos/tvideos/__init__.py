import os
from flask import Flask
from tvideos.database import DB

def create_app(test_config=None):
    print('create_app...')
    app = Flask(__name__)
    # base de datos
    DB.init(app)
    # testing
    # from tvideos.models.pelicula import Pelicula
    # pelicula = Pelicula(titulo='monito', genero='Juego', duracion=220, año=2019)
    # print(pelicula.json())
    # pelicula.insert()
    # print('query:', DB.find_one('peliculas', { 'titulo': 'monito' } ))
    
    # importar views (rutas)
    configure_blueprints(app)
    
    return app
    
def configure_blueprints(app):
    from . import views
    app.register_blueprint(views.tvideosBp)
