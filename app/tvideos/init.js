// script para iniciar la base de datos
db.peliculas.remove({});
db.clientes.remove({});
db.copias_peliculas.remove({});
db.prestamos_peliculas.remove({});
db.pagos.remove({});
db.costos_y_descuentos.remove({});

// peliculas
db.peliculas.insert(
  {
    "titulo": "Stand By Me",
    "genero": "Aventura",
    "duracion": 182,
    "año": 1986,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Wil Wheaton",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 10
  });
db.peliculas.insert(
  {
    "titulo": "Highlander",
    "genero": "Acción",
    "duracion": 192,
    "año": 1986,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Christopher Lambert",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 10
  });
db.peliculas.insert(
  {
    "titulo": "Eliminators",
    "genero": "Acción",
    "duracion": 201,
    "año": 1986,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Andrew Prine",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 10
  });
db.peliculas.insert(
  {
    "titulo": "Murphy's Law",
    "genero": "Aventura",
    "duracion": 177,
    "año": 1986,
    "nominaciones": [
      {
        "nombre_premio": "Oscar",
        "detalle": "Mejor película de aventura",
        "año": 1986,
        "ganado": false
      },
      {
        "nombre_premio": "Globo de Oro",
        "detalle": "Mejor película de aventura",
        "año": 1987,
        "ganado": true
      }
    ],
    "actores_principales": [
      {
        "nombre": "Charles Bronson",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 12
  });
db.peliculas.insert(
  {
    "titulo": "Ghost",
    "genero": "Drama",
    "duracion": 196,
    "año": 1990,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Patrick Swayze",
        interpretacion: "Protagonista"
      },
      {
        "nombre": "Hayce Tweinn",
        interpretacion: "Reparto"
      }
    ],
    "costo_unitario": 20
  });
db.peliculas.insert(
  {
    "titulo": "La Femme Nikita",
    "genero": "Acción",
    "duracion": 155,
    "año": 1990,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Anne Parillaud",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 11
  });
db.peliculas.insert(
  {
    "titulo": "Sleeping with the Enemy",
    "genero": "Drama",
    "duracion": 165,
    "año": 1991,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Julia Roberts",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 14
  });
db.peliculas.insert(
  {
    "titulo": "Showdown in Little Tokyo",
    "genero": "Acción",
    "duracion": 147,
    "año": 1991,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Dolph Lundgren",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 10
  });
db.peliculas.insert(
  {
    "titulo": "Wayne's World",
    "genero": "Comedia",
    "duracion": 157,
    "año": 1992,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Mike Myers",
        interpretacion: "Protagonista"
      },
      {
        "nombre": "Helio Hugales",
        interpretacion: "Reparto"
      }
    ],
    "costo_unitario": 6
  });
db.peliculas.insert(
  {
    "titulo": "Mom and Dad Save the World",
    "genero": "Aventura",
    "duracion": 177,
    "año": 1992,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Teri Garr",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 6
  });
db.peliculas.insert(
  {
    "titulo": "Raising Cain",
    "genero": "Crimen",
    "duracion": 201,
    "año": 1992,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "John Ligthgow",
        interpretacion: "Protagonista"
      }
    ]
  });
db.peliculas.insert(
  {
    "titulo": "Needful Things",
    "genero": "Crimen",
    "duracion": 181,
    "año": 1993,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Max von Sywdow",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 20
  });
db.peliculas.insert(
  {
    "titulo": "The Dark Half",
    "genero": "Horror",
    "duracion": 149,
    "año": 1986,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Timothy Hutton",
        interpretacion: "Protagonista"
      },
      {
        "nombre": "Tollin Navarre",
        interpretacion: "Semiprotagonista"
      }
    ],
    "costo_unitario": 13
  });
db.peliculas.insert(
  {
    "titulo": "Swing Kids",
    "genero": "Drama",
    "duracion": 132,
    "año": 1993,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Robert Sean Leonard",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 20
  });
db.peliculas.insert(
  {
    "titulo": "Sugar Hill",
    "genero": "Drama",
    "duracion": 120,
    "año": 1993,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Wesley Spines",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 10
  });
db.peliculas.insert(
  {
    "titulo": "Reality Bites",
    "genero": "Comedia",
    "duracion": 117,
    "año": 1994,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Wiona Ryder",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 10
  });
db.peliculas.insert(
  {
    "titulo": "Star Trek: Generations",
    "genero": "Acción",
    "duracion": 153,
    "año": 1994,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Patrick Stewart",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 12
  });
db.peliculas.insert(
  {
    "titulo": "Clerks",
    "genero": "Comedia",
    "duracion": 99,
    "año": 1994,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Brian O'Halloran",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 4
  });
db.peliculas.insert(
  {
    "titulo": "Intersection",
    "genero": "Drama",
    "duracion": 102,
    "año": 1994,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Richard Gere",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 4
  });
db.peliculas.insert(
  {
    "titulo": "GoldenEye",
    "genero": "Acción",
    "duracion": 97,
    "año": 1995,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Pierce Bronsnan",
        interpretacion: "Protagonista"
      },
      {
        "nombre": "Hueli Wnaotn",
        interpretacion: "Reparto"
      },
      {
        "nombre": "Dull Monteagudo",
        interpretacion: "Co-protagonista"
      },
    ],
    "costo_unitario": 8
  });
db.peliculas.insert(
  {
    "titulo": "Pocahontas",
    "genero": "Animación",
    "duracion": 88,
    "año": 1995,
    "nominaciones": [
      {
        "nombre_premio": "Oscar",
        "detalle": "Mejor película animada",
        "año": 1995,
        "ganado": true
      }
    ],
    "actores_principales": [
      {
        "nombre": "Mel Gibson",
        interpretacion: "Voz principal"
      }
    ],
    "costo_unitario": 7
  });
db.peliculas.insert(
  {
    "titulo": "Rob Roy",
    "genero": "Aventura",
    "duracion": 85,
    "año": 1995,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Liam Neeson",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 3
  });
db.peliculas.insert(
  {
    "titulo": "Funny Bones",
    "genero": "Comedia",
    "duracion": 76,
    "año": 1995,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Jerry Lewis",
        interpretacion: "Protagonista"
      }
    ],
    "costo_unitario": 5
  });
db.peliculas.insert(
  {
    "titulo": "The Hunchback of Notre Dame",
    "genero": "Animación",
    "duracion": 82,
    "año": 1986,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Demi Moore",
        interpretacion: "Protagonista"
      },
      {
        "nombre": "Asbel Veliuno",
        interpretacion: "Voz apoyo"
      }
    ],
    "costo_unitario": 8
  });
db.peliculas.insert(
  {
    "titulo": "Fire Down Below",
    "genero": "Aventura",
    "duracion": 98,
    "año": 1997,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Steven Seagal",
        interpretacion: "Protagonista"
      }
    ],
    "compañía": "Warner Bros",
    "costo_unitario": 3
  });
db.peliculas.insert(
  {
    "titulo": "Air Bud",
    "genero": "Acción",
    "duracion": 84,
    "año": 1997,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "",
        interpretacion: "Protagonista"
      }
    ],
    "compañía": "Walt Disney Pictures"
  });
db.peliculas.insert(
  {
    "titulo": "Brother",
    "genero": "Crimen",
    "duracion": 100,
    "año": 2000,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Takesi Kitano",
        interpretacion: "Protagonista"
      }
    ],
    "compañía": "Recorded Picture Company (RPC)"
  });
db.peliculas.insert(
  {
    "titulo": "Murphy's Law",
    "genero": "Aventura",
    "duracion": 177,
    "año": 1986,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Charles Bronson",
        interpretacion: "Protagonista"
      }
    ]
  });
db.peliculas.insert(
  {
    "titulo": "The Specials",
    "genero": "Acción",
    "duracion": 96,
    "año": 200,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Rob Lowe",
        interpretacion: ""
      }
    ]
  });
db.peliculas.insert(
  {
    "titulo": "Come Undone",
    "genero": "Drama",
    "duracion": 74,
    "año": 2000,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "Jaramie Elkam",
        interpretacion: "Protagonista"
      },
      {
        "nombre": "Mertoi Blooa Janhes",
        interpretacion: "Co-Protagonista"
      },
    ],
    "costo_unitario": 14
  });
db.peliculas.insert(
  {
    "titulo": "Star Wars: Episode II - Attack of the Clones",
    "genero": "Acción",
    "duracion": 154,
    "año": 2002,
    "nominaciones": [
      {
        "nombre_premio": "Oscar",
        "detalle": "Mejor película animada",
        "año": 2002,
        "ganado": true
      },
      {
        "nombre_premio": "Oscar",
        "detalle": "Mejor banda sonora",
        "año": 2002,
        "ganado": false
      }
    ],
    "actores_principales": [
      {
        "nombre": "Hayden Christensen",
        interpretacion: "Protagonista"
      }
    ],
    "compañía": "Lucasfilm",
    "costo_unitario": 15
  });
db.peliculas.insert(
  {
    "titulo": "Sonny",
    "genero": "Crimen",
    "duracion": 103,
    "año": 2002,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "James Franco",
        interpretacion: "Protagonista"
      }
    ],
    "compañía": "Gold Circle Films",
    "costo_unitario": 29
  });
db.peliculas.insert(
  {
    "titulo": "Final Destination 2",
    "genero": "Horror",
    "duracion": 88,
    "año": 2003,
    "nominaciones": [],
    "actores_principales": [
      {
        "nombre": "A.J. Cook",
        interpretacion: "Protagonista"
      }
    ],
    "compañía": "New Line Cinema",
    "costo_unitario": 6
  });

// clientes
db.clientes.insert({
  "nombres": "Alberto",
  "ap_paterno": "Perez",
  "ap_materno": "Atahuachi",
  "ci": "858181283lp",
  "no_targeta": "2cxxc",
  "fecha_nacimiento": "23/11/1988",
  "email": "Jhonce@51235.com",
  "direccion": "",
  "geolocalizacion": "-16.23123,-68.31232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Lucas",
  "ap_paterno": "Morales",
  "ap_materno": "Morales",
  "ci": "688102213",
  "no_targeta": "775112",
  "fecha_nacimiento": "23/01/1968",
  "email": "asdfasdf2@faa.com",
  "direccion": "",
  "geolocalizacion": "-16.23123,-68.31232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Monica",
  "ap_paterno": "Lauhio",
  "ap_materno": "Lara",
  "ci": "9160244",
  "no_targeta": "6x523vq..21",
  "fecha_nacimiento": "14/12/1978",
  "email": "58182.coae@6562.com",
  "direccion": "5124#eq .com",
  "geolocalizacion": "-16.9000,-68.51232",
  "betado": true
});
db.clientes.insert({
  "nombres": "Lucía Mercedes",
  "ap_paterno": "Mamani",
  "ap_materno": "Paller",
  "ci": "2458851",
  "no_targeta": "9016-1238541",
  "fecha_nacimiento": "14/12/1978",
  "email": "58182.coae@6562.com",
  "direccion": "5124#eq .com",
  "geolocalizacion": "-16.9000,-68.51232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Sonia Emilia",
  "ap_paterno": "Torres",
  "ap_materno": "Castillo",
  "ci": "6771432",
  "no_targeta": "98514c32",
  "fecha_nacimiento": "14/12/1981",
  "email": "58182.coae@6562.com",
  "direccion": "Av. Mariscal Sant Cruz 35112",
  "geolocalizacion": "-16.2200,-68.1932",
  "betado": false
});
db.clientes.insert({
  "nombres": "Fanny",
  "ap_paterno": "Nuna",
  "ap_materno": "Tellez",
  "ci": "9986181",
  "no_targeta": "81851124",
  "fecha_nacimiento": "05/09/1991",
  "email": "fanuna@cne.org",
  "direccion": "Av. Estellez No. 901192",
  "geolocalizacion": "-16.8000,-68.51232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Antonia Carmen",
  "ap_paterno": "Salles",
  "ap_materno": "Torrico",
  "ci": "54816253",
  "no_targeta": "88114c",
  "fecha_nacimiento": "05/11/1992",
  "email": "tsalles@cee.org",
  "direccion": "Av. Estellez No. 901192",
  "geolocalizacion": "-16.8300,-68.51232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Eliza Sabrina",
  "ap_paterno": "León",
  "ap_materno": "Gonazles",
  "ci": "96138744",
  "no_targeta": "85180912090",
  "fecha_nacimiento": "12/06/1990",
  "email": "terr.co@cee.org",
  "direccion": "Av. Platter No. 901192",
  "geolocalizacion": "-16.9300,-68.41232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Danilo",
  "ap_paterno": "Hernández",
  "ap_materno": "Derulo",
  "ci": "9041511",
  "no_targeta": "9951cda5",
  "betado": false
});
db.clientes.insert({
  "nombres": "Milton",
  "ap_paterno": "Casas",
  "ap_materno": "Hunanchuna",
  "ci": "58883092c",
  "no_targeta": "2cxxc",
  "betado": false
});
db.clientes.insert({
  "nombres": "Ernestino",
  "ap_paterno": "Salgoz",
  "ap_materno": "Glence",
  "ci": "77671695",
  "no_targeta": "901v815",
  "betado": false
});
db.clientes.insert({
  "nombres": "Rigoberto Alfonsino",
  "ap_paterno": "Perez",
  "ap_materno": "Graces",
  "ci": "9861n214",
  "no_targeta": "30007600602",
  "betado": false
});
db.clientes.insert({
  "nombres": "Alicia",
  "ap_paterno": "Montaño",
  "ap_materno": "Martinez",
  "ci": "356727447",
  "no_targeta": "861n3419",
  "betado": false
});
db.clientes.insert({
  "nombres": "Oscar Sergio",
  "ap_paterno": "Sandoval",
  "ap_materno": "Huanca",
  "ci": "98245333",
  "no_targeta": "851c16512",
  "betado": false
});
db.clientes.insert({
  "nombres": "Shayla",
  "ap_paterno": "Terrent",
  "ap_materno": "Bell",
  "ci": "UU85121-51",
  "no_targeta": "111108851n41",
  "betado": false
});
db.clientes.insert({
  "nombres": "Teresa",
  "ap_paterno": "Lima",
  "ap_materno": "Saldoval",
  "ci": "7672172374",
  "no_targeta": "994011",
  "betado": false
});
db.clientes.insert({
  "nombres": "Roberto",
  "ap_paterno": "Garcia",
  "ap_materno": "Vera",
  "ci": "66573952",
  "no_targeta": "4124tvc13214a",
  "betado": true
});
db.clientes.insert({
  "nombres": "Gil Ali",
  "ap_paterno": "Lua",
  "ap_materno": "Chrep",
  "ci": "761944865",
  "no_targeta": "8127509123857",
  "betado": false
});
db.clientes.insert({
  "nombres": "Fausto Ernesto",
  "ap_paterno": "Quiroga",
  "ap_materno": "Zudañez",
  "ci": "667572157",
  "no_targeta": "1765103x1122",
  "fecha_nacimiento": "25/02/1986",
  "email": "ce23@cee.org",
  "direccion": "Av. Diamente No. 9941",
  "geolocalizacion": "-16.9300,-68.21232",
  "betado": false
});
db.clientes.insert({
  "nombres": "Paola Paloma",
  "ap_paterno": "Vega",
  "ap_materno": "Saneli",
  "ci": "678268620",
  "no_targeta": "8813c1245",
  "betado": false
});

// Copias de peliculas
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "La Femme Nikita"}).toArray()[0]._id,
  creacion: "2020/03/14 19:20:00",
  bajas: [
    {
      razon: "Prohibicion gobernacion La Paz",
      fecha: "2020/03/19 12:04:02"
    }
  ],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "The Specials"}).toArray()[0]._id,
  creacion: "2020/02/02 20:20:20",
  bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Sugar Hill"}).toArray()[0]._id,
    creacion: "2020/02/02 20:20:20",
  bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo:"Showdown in Little Tokyo"}).toArray()[0]._id,
    creacion: "2020/02/02 20:20:20",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "The Specials"}).toArray()[0]._id,
    creacion: "2020/02/02 20:20:20",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Swing Kids"}).toArray()[0]._id,
    creacion: "2020/02/03 17:20:21",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "The Hunchback of Notre Dame"}).toArray()[0]._id,
    creacion: "2020/02/02 20:20:21",
    bajas: [],
  prestado: false
});

db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Needful Things"}).toArray()[0]._id,
    creacion: "2019/12/29 10:00:50",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Needful Things"}).toArray()[0]._id,
    creacion: "2019/12/29 10:00:51",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Intersection"}).toArray()[0]._id,
    creacion: "2019/12/29 10:00:50",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Fire Down Below"}).toArray()[0]._id,
    creacion: "2019/12/29 11:00:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Air Bud"}).toArray()[0]._id,
    creacion: "2019/12/29 10:00:50",
    bajas: [],
  prestado: false
});

db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "GoldenEye"}).toArray()[0]._id,
  creacion: "2020/03/01 19:20:00",
  bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "GoldenEye"}).toArray()[0]._id,
  creacion: "2029/03/01 19:20:00",
  bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "GoldenEye"}).toArray()[0]._id,
  creacion: "2029/03/01 19:14:00",
  bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "GoldenEye"}).toArray()[0]._id,
  creacion: "2029/03/01 19:14:10",
  bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Final Destination 2"}).toArray()[0]._id,
    creacion: "2019/12/30 09:20:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Final Destination 2"}).toArray()[0]._id,
    creacion: "2019/12/30 09:39:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Stand By Me"}).toArray()[0]._id,
    creacion: "2019/12/29 16:09:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Highlander"}).toArray()[0]._id,
    creacion: "2019/12/29 16:09:01",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "Highlander"}).toArray()[0]._id,
  creacion: "2019/12/29 16:09:02",
  bajas: [
    {
      razon: "Robo",
      fecha: "2020/01/23 17:33:09",
      cliente: db.clientes.find({ ci: "688102213" })
    }
  ]
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Clerks"}).toArray()[0]._id,
    creacion: "2019/12/28 19:11:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Clerks"}).toArray()[0]._id,
    creacion: "2019/12/28 19:11:01",
    bajas: []
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Pocahontas"}).toArray()[0]._id,
    creacion: "2020/02/23 19:20:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Pocahontas"}).toArray()[0]._id,
    creacion: "2020/02/23 19:20:00",
    bajas: []
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Rob Roy"}).toArray()[0]._id,
    creacion: "2020/02/11 14:00:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "The Hunchback of Notre Dame"}).toArray()[0]._id,
  creacion: "2020/02/11 14:00:30",
  bajas: [],
  prestado: true
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Fire Down Below"}).toArray()[0]._id,
    creacion: "2020/02/11 14:00:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Air Bud"}).toArray()[0]._id,
    creacion: "2020/02/11 14:00:35",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
  pelicula: db.peliculas.find({ titulo: "Brother"}).toArray()[0]._id,
  creacion: "2020/02/11 14:00:00",
  bajas: [],
  prestado: true
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "Funny Bones"}).toArray()[0]._id,
    creacion: "2020/02/12 11:20:00",
    bajas: [],
  prestado: false
});
db.copias_peliculas.insert({
    pelicula: db.peliculas.find({ titulo: "La Femme Nikita"}).toArray()[0]._id,
    creacion: "2019/03/14 19:20:00",
    bajas: [
        {
            razon: "Prohibicion gobernacion La Paz",
            fecha: "2020/03/19 12:04:11"
        }
    ],
  prestado: false
});

// prestamos
db.prestamos_peliculas.insert({
  cliente: db.clientes.find({ ci: "98245333" }).toArray()[0]._id,
  copias_peliculas: [
    {
      copia_pelicula: db.copias_peliculas.aggregate([
	{
	  $lookup: {
	    from: 'peliculas',
	    localField: 'pelicula',
	    foreignField: '_id',
	    as: 'pelicula'
	  }
	},
        {
	  $match: { 'pelicula.titulo': 'Highlander' }
	}
      ]).toArray()[0]._id
    }
  ],
  fecha_prestamo: "20/03/2020 14:00:14",
  fecha_devolucion: "",
  dias_prestamo: 5,
  retornado: false,
  creacion: '20/03/2020 14:00:14 '
});
db.prestamos_peliculas.insert({
  cliente: db.clientes.find({ ci: "58883092c" }).toArray()[0]._id,
  copias_peliculas: [
    {
      copia_pelicula: db.copias_peliculas.aggregate([
	{
	  $lookup: {
	    from: 'peliculas',
	    localField: 'pelicula',
	    foreignField: '_id',
	    as: 'pelicula'
	  }
	},
        {
	  $match: { 'pelicula.titulo': 'The Hunchback of Notre Dame' }
	}
      ]).toArray()[0]._id
    }
  ],
  fecha_prestamo: "14/03/2020 14:00:14",
  fecha_devolucion: "16/03/2020 17:11:23",
  creacion: "2020/03/14 14:00:14",
  dias_prestamo: 2,
  retornado: true,
  pago: {
    fecha_facturacion: "2020/03/16 17:10:23",
    monto: 5
  }
});

// costos prestamos
db.costos_y_descuentos.insert({
  costos: [
    {
      cantidad: 1,
      costo: 3
    },
    {
      cantidad: 2,
      costo: 4
    },
    {
      cantidad: 3,
      costo: 5
    },
    {
      cantidad: 4,
      costo: 6
    },
    {
      cantidad: 5,
      costo: 6.5
    },
  ],
  descuentos: [
    {
      cantidad_minima: 3,
      cantidad_maxima: 5,
      descuento: 0.05
    },
    {
      cantidad_minima: 5,
      cantidad_maxima: 9000,
      descuento: 0.10
    },
  ]
});
