## Video Tutorial

- https://www.youtube.com/watch?v=ugxt0QUUSt0

## Instalar Docker

- ./instalar-docker-ubuntu.sh

## Instalar docker-compose
```bash
- ./instalar-docker-compose.sh
```
## Configurar mongo replicaSet + app
```bash
docker-compose build
```
```bash
docker images
```
```bash
docker-compose up -d
```

## Conectarse a mongo (Master)
- docker-compose exec mongo-rs0-1 mongo

## Revisar la configuración
```bash
> rs.status().members
```



