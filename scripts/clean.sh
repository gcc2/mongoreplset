#!/bin/bash
#for ((i=0; i<255;i++)) do docker compose status; sleep 5; done
if [[ $(docker ps -aq) ]]; then
    docker rm $(docker ps -aq) && echo "docker rm OK"
else
    echo "docker rm no."
fi

if [[ $(docker images --filter=reference=app1 --format "{{.ID}}") ]]; then
    docker rmi $(docker images --filter=reference=app1 --format "{{.ID}}") && echo "docker rm app1 OK"
else
    echo " docker image app1 rm no."
fi

if [[ $(docker images --filter=reference=mongo-start --format "{{.ID}}") ]]; then
    docker rmi $(docker images --filter=reference=mongo-start --format "{{.ID}}") && echo "docker rm mongo-start OK"
else
    echo " docker image mongo-start rm no."
fi

if [[ $(docker images --filter=reference=setup-rs --format "{{.ID}}") ]]; then
    docker rmi $(docker images --filter=reference=setup-rs --format "{{.ID}}") && echo "docker rm setup-rs OK"
else
    echo " docker image setup-rs rm no."
fi

#docker rmi $(docker images --filter=reference=setup-rs --format "{{.ID}}")
#docker rmi $(docker images --filter=reference=mongo-start --format "{{.ID}}")
echo "gunchelo" | sudo -S rm -rf /home/jenkins/workspace/mongors/mongo-rs0-*/data/*


